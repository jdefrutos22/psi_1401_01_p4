# lucnh this script and as mouse first clean orphan games and then
# join the game
import os,django
os.environ['DJANGO_SETTINGS_MODULE'] =  'RatonGato.settings'
django.setup()
from django.core.urlresolvers import reverse
from django.contrib.auth.models import User
from django.test import Client
from server.models import Game, Move, Counter
import time
import re, math
import random, json

#python ./manage.py test rango.tests.UserAuthenticationTests --keepdb
#class UserAuthenticationTests(TestCase):

usernameCat = 'gatoUser'
passwdCat = 'gatoPasswd'
usernameMouse = 'ratonUser'
passwdMouse = 'ratonPasswd'
DEBUG = False

class CatPlayer():
    def __init__(self):
        self.clientCat = Client()
        self.cats = [0, 2, 4, 6]
        self.game=None
        currenTime = time.time()
        RandomSeed = int(currenTime) + os.getpid()
        random.seed(RandomSeed)

    def creatUser(self, userName, userPassword):
        try:
            user = User.objects.get(username=userName)
        except User.DoesNotExist:
            user = User(username=userName, password=userPassword)
            user.set_password(user.password)
            user.save()
        return user.id

    def login(self, userName, userPassword, client):
        response = client.get(reverse('login_user'))
        loginDict={}
        loginDict["username"]=userName
        loginDict["password"]=userPassword
        response = client.post(reverse('login_user'), loginDict, follow=True)
        return response


    def deleteUser(self, userName):
        try:
            userKK = User.objects.get(username=userName)
            userKK.delete()
        except User.DoesNotExist:
            pass

    def logout(self, client):
        try:
            response = client.get(reverse('logout_user'), follow=True)
            return response
        except:
            pass

    def wait_loop(self, seconds):
        key = "It is your turn : True"
        print ("wait loop begin")
        while True:
            response = self.clientCat.get(reverse('status_turn'))
            if response.content.find(key) != -1:
                break
            time.sleep(seconds)
            print ".",
        print ("wait loop end")
        return 0

    def parse_mouse(self):
        """http://pythex.org/"""
        response = self.clientCat.get(reverse('status_board'))
        searchString='<td id=id_(\d+) onclick="getID\(this\);" >\s+&#9920;'
                       #<td id=id_0 onclick="getID(this);" >\n              &#9922;
        finds = re.findall(searchString,response.content)
        print response.content
        print finds
        return finds[0]

    def dist(self, p1,p2):
        pos1 = int(p1); pos2 = int(p2)
        x1 = pos1%8
        y1 = pos1//8

        x2 = pos2%8
        y2 = pos2//8

        return (x1-x2)**2+(y1-y2)**2

    def catAMayorDist(self, mouseposition):
        d=-1
        cat= None
        for i in range(4):
            gato = self.cats[i]
            if d<self.dist(gato, mouseposition):
                d = self.dist(gato, mouseposition)
                cat = self.cats[i]
        return  cat

    def catAMayorDistSameCol(self, mouseposition,col):
        d=-1
        cat= None
        for i in range(4):
            gato = self.cats[i]
            if d<self.dist(gato, mouseposition) and int(gato)//8==int(col):
                d = self.dist(gato, mouseposition)
                cat = self.cats[i]
        return  cat

    def getNextCat(self):
        minimo = 64
        for i in range(4):
            m  = self.cats[i]
            if minimo > m:
                minimo = m
        return minimo

    def twoOfDif(self,mouseposition):
        cat = int(self.getNextCat())
        col1 = cat//8; col2 = int(mouseposition)//8
        if col2 - col1>2:
            return True
        return False


    def validRandomMove(self,mousePosition):

        cat = self.getNextCat()
        moveDict = {}

        if int(cat)//8 > int(mousePosition)//8:
            print "gana el raton"
            exit(0)

        if int(cat)//8 == 7:
            cat = self.getNextCat()
            print "Los gatos llegaron al final del tablero"
            exit(0)

        if not self.twoOfDif(mousePosition) : 
            cat = self.catAMayorDistSameCol(mousePosition, int(cat//8))

        while True:

            if cat%2 == 0:
                target = cat+9
            else: 
                target = cat+7

            if int(mousePosition) == int(target):
                    cat = self.catAMayorDist(mousePosition)
                    continue

            moveDict['origin']=cat
            moveDict['target']=target
            response = CatPlayer.clientCat.post(reverse('move'), moveDict)
            print response.content
            responseDict = json.loads(response.content)
            if not responseDict["moveDone"]:
                continue
            else:
                break

        return cat, target


        #try to move
    def set_catPosition(self,origin, position):
        for cat in range(0,4):
            if self.cats[cat] == origin:
                self.cats[cat] = target
                break
        print("self.cats",self.cats)

CatPlayer = CatPlayer()
#make sure mouse user exists
userMouseID = CatPlayer.creatUser(usernameCat, passwdCat)
#login
response = CatPlayer.login(usernameMouse, passwdMouse,CatPlayer.clientCat)
#create game
response = CatPlayer.clientCat.get(reverse('create_game'))
#get last game id
game = Game.objects.latest("id")

#check if mouse has joined
print("waiting untill mouse joins")
while True:
     time.sleep(1)
     gameOK = Game.objects.filter(mouseUser__isnull=True).order_by("-id")
     if gameOK.exists():
         game2 = gameOK[0]
         if game.id == game2.id:
             print ".",
             continue
     else:
         break
print("game id=%d"%game.id)
moveDict={}
while True:
     # loop waiting for myTurn=True
     CatPlayer.wait_loop(1)# time in second between retrial

     #moveDict['origin']=0
     #moveDict['target']=9
     #response = CatPlayer.clientCat.post(reverse('move'), moveDict)

     # parse mouse position
     mousePosition = CatPlayer.parse_mouse()
     print('mousePosition', mousePosition)
     #create random cat move
     origin, target = CatPlayer.validRandomMove(mousePosition)
     #move
     CatPlayer.set_catPosition(origin, target)

