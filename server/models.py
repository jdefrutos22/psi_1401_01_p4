""" Modulo models.py implementa los modelos de la base de datos de
nuestro juego ratonGato """


from __future__ import unicode_literals
from django.db import models
from django.template.defaultfilters import slugify

from django.contrib.auth.models import User
from django.core.validators import MaxValueValidator

#Models here.
class Game(models.Model):
    """ Entidad Game: Representa una partida del juego.
        Atributos:
            (1)catUser: Usuario que juega como gato este juego. 
            (2)mouseUser: Usuario que juega como raton en el juego
            (3)cat1,...,cat4: gatos del juego. Reprensados mediante un entero positivo menor que 64 (posicion en el tablero.)
            (4)mouse: raton del juego. Entero de 0 a 63 que indica su posicion en el tablero.
            (5)catTurn: booleano que indica si es el turno del gato. Por defecto vale true.
    """

    catUser = models.ForeignKey(User, related_name='gamecatUsers')
    mouseUser = models.ForeignKey(User, null = True)
    cat1 = models.PositiveIntegerField(default=0, validators=[MaxValueValidator(63)])
    cat2 = models.PositiveIntegerField(default=2, validators=[MaxValueValidator(63)])
    cat3 = models.PositiveIntegerField(default=4, validators=[MaxValueValidator(63)])
    cat4 = models.PositiveIntegerField(default=6, validators=[MaxValueValidator(63)])
    mouse = models.PositiveIntegerField(default=59, validators=[MaxValueValidator(63)])
    catTurn = models.BooleanField(default=True)

    #def __str__(self): 
    #    return self.name

    #def __unicode__(self): # For Python 2, use __unicode__ too
    #    return self.name
        

class Move(models.Model):
    """ Entidad Move: Representa un movimiento en el tablero.
        Atributos:
            (1) origin: Entero de 0 a 63 que indica el origen del movimiento en el tablero.
            (2) target: Entero de 0 a 63 que indica la casilla destino del movimiento.
            (3) game: juego asociado a este movimiento.
    """
    origin = models.PositiveIntegerField(validators=[MaxValueValidator(63)])
    target = models.PositiveIntegerField(validators=[MaxValueValidator(63)])
    game = models.ForeignKey(Game)
    
    #def __str__(self): 
    #    return self.name

    #def __unicode__(self):
    #    return self.name
    
class Counter(models.Model):
    counter = models.PositiveIntegerField(default=0)

    
