"""@package docstring
    Controlador de la aplicacion web Server.
"""

from django.shortcuts import render
from django.http import HttpResponse
from django.contrib.auth import authenticate, login
from django.http import HttpResponseRedirect, HttpResponse
from django.contrib.auth.decorators import login_required
from django.contrib.auth import logout
from server.models import *
from server.forms import UserForm, Moveform
from django.shortcuts import render_to_response
import json

import time
import threading

# Create your views here.


def index(request):
    """@autor Jose Manuel de Frutos Porras.
    Llama simplemente la vista index.html.
    """
    
    return render(request, 'server/base.html', {'username':request.user.username})


def counter(request):

    #counterSession
    if 'counterSes' in request.session:
        request.session['counterSes'] += 1
    else:
        request.session['counterSes'] = 1
    counterSes = request.session['counterSes']

    #counterGlobal
    counterGlobals = Counter.objects.filter()
    if counterGlobals.exists():
        counterGlobal = counterGlobals[0]
    else:
        counterGlobal = Counter(1)

    context_dict = {}
    context_dict['counterGlobal'] = counterGlobal.counter
    context_dict['counterSes'] = counterSes
    counterGlobal.counter = counterGlobal.counter +1
    counterGlobal.save()

    if request.is_ajax():
        template = "server/game_ajax.html"
    else:
        template = "server/game.html"

    return render(request, template, context=context_dict)
    
def register_user(request):
    """@autor Jose Manuel de Frutos Porras.
    Se encarga tomar la informacion del formulario de registro y guardar el nuevo usuario en base de datos. 
    Llama a la vista register.html con la informacion correspondiente.
    Entrada: dos cadenas con el nombre del usuario y la clave
    Salida: Mensaje acusando la creacion del nuevo usuario.
    """
    #Dejamos los comentarios en ingles pues nos parecen claros pese a estar en otra lengua.

    # A boolean value for telling the template whether the registration was successful.
    # Set to False initially. Code changes value to True when registration succeeds.
    registered = False

    # If it's a HTTP POST, we're interested in processing form data.
    if request.method == 'POST':
        # Attempt to grab information from the raw form information.
        # Note that we make use of both UserForm and UserProfileForm.
        user_form = UserForm(data=request.POST)

        # If the two forms are valid...
        if user_form.is_valid():
            # Save the user's form data to the database.
            user = user_form.save()

            # Now we hash the password with the set_password method.
            # Once hashed, we can update the user object.
            user.set_password(user.password)
            user.save()

            registered = True
            return render(request, 'server/login.html', {})
        # Invalid form or forms - mistakes or something else?
        # Print problems to the terminal.
        # They'll also be shown to the user.
        else:
            print user_form.errors
    # Not a HTTP POST, so we render our form using two ModelForm instances.
    # These forms will be blank, ready for user input.
    else:
        user_form = UserForm()
        
    # Render the template depending on the context.
    return render(request, 'server/register.html', {'user_form': user_form, 'registered': registered})

def login_user(request):
    """@autor Jose Manuel de Frutos Porras.
    Se encarga tomar la informacion del formulario de login y guardarla en base de datos. 
    Llama a la vista login.html con la informacion correspondiente.
    Entrada: dos cadenas con el nombre del usuario y la clave
    """
    
    # If the request is a HTTP POST, try to pull out the relevant information.

    logout_user(request)

    if request.method == 'POST':
        # Gather the username and password provided by the user.
        # This information is obtained from the login form.
                # We use request.POST.get('<variable>') as opposed to request.POST['<variable>'],
                # because the request.POST.get('<variable>') returns None, if the value does not exist,
                # while the request.POST['<variable>'] will raise key error exception
        username = request.POST.get('username')
        password = request.POST.get('password')
        isCat = request.POST.get('isCat')

        # Use Django's machinery to attempt to see if the username/password
        # combination is valid - a User object is returned if it is.
        user = authenticate(username=username, password=password)

        # If we have a User object, the details are correct.
        # If None (Python's way of representing the absence of a value), no user
        # with matching credentials was found.
        if user:
            # Is the account active? It could have been disabled.
            if user.is_active:
                # If the account is valid and active, we can log the user in.
                # We'll send the user back to the homepage.
                login(request, user)
                if isCat == "isCat": #Si es un gato creamos el juego directamente.
                    request.session['amIcat'] = True 
                    return create_game(request)
                else:
                    request.session['amIcat'] = False   #Se une como raton, luego 'amIcat' es False.
                    return render(request, 'server/game.html', {'username':request.user.username, 'myTurn': False})
                    #return join_game(request)	
		            #return render(request, 'server/login.html', {'user':user})
            else:
                # An inactive account was used - no logging in!
                return HttpResponse("Your account is disabled.")
        else:
            # Bad login details were provided. So we can't log the user in.
            return render(request, 'server/login.html', {'error':'Ese par contrasena usuario no existe.'})

    # The request is not a HTTP POST, so display the login form.
    # This scenario would most likely be a HTTP GET.
    else:
        # No context variables to pass to the template system, hence the
        # blank dictionary object...
        return render(request, 'server/login.html', {})

# Use the login_required() decorator to ensure only those logged in can access the view.
@login_required #redirect_field_name='/server/notlogged/'
def logout_user(request):
    """@autor Jose Manuel de Frutos Porras.
    Se encarga de hacer logout. Es decir llamar a logout y
    borrar todas la variables de session.
    Llama despues a la vista logout.html con el nombre del usuario que ha hecho logout.
    Se necesita estar logeado para poder llamar a esta funcion.
    Entrada: Se hace uso de las funciones de Django para gestionar usuarios
    Accesibilidad: esta funcion solo es ejecutable por usuarios logeados en el sistema
    """
    # Since we know the user is logged in, we can now just log them out.
    username = request.user.username
    logout(request)
    for key in request.session.keys(): #Borramos las variables de session.
        del request.session[key]

    return render(request, 'server/logout.html', {'username': username})

def nologged(request):
    """@autor Jose Manuel de Frutos Porras.
    Llama a la vista nologged.html
    """
    return  render(request, 'server/nologged.html', {})

@login_required
def create_game(request):
    """@autor Jose Manuel de Frutos Porras.
    Controlador. Crea un juego e inicializa las variables de session correspondientes.
    'gameID' inicializada al id del juego que se crea y 'amIcat' inicializada a True.
    Se necesita estar logeado para poder llamar a esta funcion.
    """
    context_dict = {}
    context_dict['username'] = request.user;
    request.session['amIcat'] = True 

    game = Game(catUser = request.user) #Creamos el juego.
    game.catTurn = True #Empiezan los gatos.
    game.save()

    context_dict['game'] = game

    request.session['gameID'] = game.id

    context_dict['myTurn'] = True

    return render(request, 'server/game.html', context_dict)


def clean_orphan_games(request):
    """@autor Jose Manuel de Frutos Porras.
    Se borran todos los juegos que no tengan un segundo usuario asignado.
    Se llama posteriormente a la vista clean.html con el numero de juegos borrados.
    Se necesita estar logeado para poder llamar a esta funcion.
    Entrada: ninguna
    Salida: valor = numero de juegos borrados
    """

    orphan_games = Game.objects.filter(mouseUser__isnull=True) #juegos sin mouseUser Asignado

    if not orphan_games.exists():
        return  render(request, 'server/clean.html',  {'rows_count':0})

    i = 0 #Contamos los juegos borrados.
    for game in orphan_games :
        game.delete()
        i+=1

    return  render(request, 'server/clean.html',  {'rows_count':i})

@login_required
def join_game(request):

    """@autor Jose Manuel de Frutos Porras.
    Busca el ultimo juego con un solo usuario asignado y si existe un
    usuario logeado se asigna como usuario raton. Almacena el identicador de juego 
    en una variable de sesion llamada gameID.
    Se llama posteriormente a la vista join.html con el el resultado del join y el juego al que se ha unido si es asi.
    Se necesita estar logeado para poder llamar a esta funcion.
    Entrada: ninguna.
    Salida: Mensaje comunicando que el juego puede empezar. Incluid el nombre
    de los usuarios gato y raton asi como sus ids en el mensaje. Igualmente incluid
    el id del juego.
    Accesibilidad: esta funcion solo es ejecutable por usuarios logeados en el sistema
    """

    orphan_games = Game.objects.filter(mouseUser__isnull=True).order_by('-id') #juegos sin mouseUser Asignado ordenados de mayor a menor por id.
    while not orphan_games.exists(): #no hay juegos disponibles.
        time.sleep(2)
        orphan_games = Game.objects.filter(mouseUser__isnull=True).order_by('-id')
    
    request.session['amIcat'] = False    
    game = orphan_games[0] #el primero es el juego huerfano con mayor id.

    game.mouseUser = request.user
    game.save()

    request.session['gameID'] = game.id
    return status_board(request)



def is_finish(request):

    list_games = Game.objects.filter(id = request.session['gameID'])
        
    game = list_games[0]

    finish = True
    wins=None
    for move in [-9, -7, +7, +9]:
        context_dict = is_move_valid(game.mouse, game.mouse+move, game, False)
        if context_dict['moveDone'] == True:
            finish = False
            break
        wins= "Cats wins!"
    
    if finish == True:
        return {'finish': finish, 'wins': wins}

    if game.cat1//8 >= game.mouse//8 and game.cat2//8 >= game.mouse//8 and game.cat3//8 >= game.mouse//8 and game.cat4//8 >= game.mouse//8:
        finish=True
        wins= "Mouse wins!"
    else:
        finish = False
    return {'finish': finish, 'wins': wins}

@login_required
def move(request):
    """@autor Jose Manuel de Frutos Porras.
    Este es una funcion auxiliar que llama bien a mouse_move
    o a cat move dependiendo de a quien le toque jugar. Usa game.catTurn
    y la variable gameId almacenada como variable de sesion.
    Se llama posteriormente a la vista move.html.
    Devuelve  el  juego  y  el  movimiento  efectuados.  Asi  como  un  mensaje
    de error y la variable moveDone = True si el movimiento se creo problemas o
    moveDone=False en caso contrario.
    Se necesita estar logeado para poder llamar a esta funcion.
    """

    # If the request is a HTTP POST, try to pull out the relevant information.
    if request.method == 'POST':
        move_form = Moveform(request.POST)

        if move_form.is_valid:
            origin = request.POST.get('origin')
            final = request.POST.get('target')

            if origin is None: #Vemos que los campos introducidos no son vacios.
                return render(request, 'server/move.html',{'moveDone':False, 'error':"Introduce an origin", 'game':game, 'move':None} )

            if final is None:
                render(request, 'server/move.html',{'moveDone':False, 'error':"Introduce a target", 'game':game, 'move':None} )     
            
            origin = int(origin) #Si los datos introducidos no son vacios casteamos a entero.
            final = int(final)

            list_games = Game.objects.filter(id=request.session['gameID'])

            if not list_games.exists():
                return render(request, 'server/move.html',{'moveDone':False, 'error':"Cannot create a move", 'game':game, 'move':None} )
            
            game = list_games[0]    
            
            if request.session['amIcat']:                                           #Si el usuario es el gato llama a cat_move
                context_dict = cat_move(origin, final, request.session['gameID'])
            else:                                                                   #Si el usuario es el raton llama a mouse_move
                context_dict = mouse_move(origin, final, request.session['gameID'])

            dic = is_finish(request)
            context_dict['finish'] = dic['finish']; context_dict['wins'] = dic['wins'];
            return HttpResponse(json.dumps({'moveDone': context_dict['moveDone'], 'error': context_dict['error'], 'finish':context_dict['finish'], 'wins': context_dict['wins']}), content_type='application/json')
    else:
        return HttpResponse('Movimiento invalido')
    
    
def cat_move(origin, final, gameID):
    """@autor Jose Manuel de Frutos Porras
    Si game.catTurn==True almacena el movimiento del gato. Actualiza
    el objeto de tipo Game (posicion de uno de los gatos y catTurn) y crea un objeto
    de tipo Move. Se debe comprobar que el movimiento es valido.

    Entrada: posicion inicial y  nal del movimiento del gato. Usa el identicador
    de juego almacenados como variable de sesion.

    Salida: Esta funcion no se llamara desde el navegador sino desde una funcion
    auxiliar llamada move. Devuelve en un diccionario: el juego, el movimiento, un
    mensaje de error y un booleano que indica si el movimiento se ha creado (True)
    o si ha habido algun problema (False)

    Accesibilidad: no mapeada con un URL. Esto es, no incluidla en
    urls.py

    """
    context_dict = {}
    list_games = Game.objects.filter(id=gameID)
        
    game = list_games[0]

    if game.mouseUser is None:
        return {'moveDone':False, 'error':"Cannot create a move: I valid game requires a mouse user", 'game':game, 'move':None}
        
    if not game.catTurn: #No es tu turno. Es el turno del raton
        return {'moveDone':False, 'error':"No es tu turno", 'game':game, 'move':None}
       
    context_dict = is_move_valid(origin, final, game, True) 
    if not context_dict['moveDone']:
        context_dict['game'] = game
        context_dict['move'] = None
        return context_dict
    
    #Actualizamos la base de datos.
    move = Move(origin=origin, target=final, game=game)
    move.save()

    set_cat_final(origin, final, game)
    game.catTurn = not game.catTurn
    game.save()
    
    #Devolvemos el context_dict actualizado.
    context_dict['error'] = ""
    context_dict['game'] = game
    context_dict['move'] = move    
    return context_dict

       
def mouse_move(origin, final, gameID):
    """@autor Jose Manuel de Frutos Porras
    Si game.catTurn==False almacena el movimiento del gato. Actualiza
    el objeto de tipo Game (posicion de uno de los gatos y catTurn) y crea un objeto
    de tipo Move. Se debe comprobar que el movimiento es valido.

    Entrada: posicion inicial y  nal del movimiento del raton. Usa el identicador
    de juego almacenados como variable de sesion.

    Salida: Esta funcion no se llamara desde el navegador sino desde una funcion
    auxiliar llamada move. Devuelve en un diccionario: el juego, el movimiento, un
    mensaje de error y un booleano que indica si el movimiento se ha creado (True)
    o si ha habido algun problema (False)

    Accesibilidad: no mapeada con un URL. Esto es, no incluidla en
    urls.py

    """
    context_dict = {}
    list_games = Game.objects.filter(id=gameID)
        
    game = list_games[0]

    if game.mouseUser is None:
        return {'moveDone':False, 'error':"Cannot create a move: I valid game requires a mouse user", 'game':game, 'move':None}
        
    if game.catTurn: #No es tu turno. Es el turno del gato.
        return {'moveDone':False, 'error':"No es tu turno", 'game':game, 'move':None}
    

    context_dict = is_move_valid(game.mouse, final, game, False)
    if not context_dict['moveDone']:
        context_dict['game'] = game
        context_dict['move'] = None
        return context_dict

    #Actualizamos la base de datos.
    move = Move(origin=origin, target=final, game=game)
    move.save()

    game.mouse =final
    game.catTurn = not game.catTurn
    game.save()

    #Devolvemos el context_dict actualizado.
    context_dict['error'] = ""
    context_dict['game'] = game
    context_dict['move'] = move
    return context_dict


def is_move_valid(origin, final, game, isCat):
    """@autor Jose Manuel de Frutos Porras
    Funcion que comprueba si el movimiento es valido. Funcion auxiliar.
    Entrada: origen, destino del mov, juego y si es gato o raton el que mueve.
    Salida moveDone= true si se ha podido realizar el mov y un mensaje de error.
    """
    context_dict = {}
    #El movimiento es realizado por el gato
    if isCat: 
        MOVE_LEFT = 7 
        MOVE_RIGHT = 9
        cat = get_cat(origin, game)
        if is_catThere(final, game) :
            context_dict['moveDone'] = False
            context_dict['error'] = 'cat'
            return context_dict
        if cat is None:
            context_dict['moveDone'] = False
            context_dict['error'] = 'Movimiento invalido'
            return context_dict
        if is_catThere(final,game): #No se puede mover por que ya hay alguien en esa casilla
            context_dict['moveDone'] = False
            context_dict['error'] = 'Movimiento invalido'
            return context_dict
        if is_border_top(cat):
            context_dict['moveDone'] = False
            context_dict['error'] = 'Movimiento invalido'
            return context_dict
        if is_border_left(cat):  #El gato que va ha mover esta en la fila inferior del tablero pegado al border izquierdo
            context_dict['moveDone'] = (final==cat+MOVE_RIGHT)
            context_dict['error'] = 'Movimiento invalido'
            return context_dict
        if is_border_right(cat):
            context_dict['moveDone'] = (final==cat+MOVE_LEFT)
            context_dict['error'] = 'Movimiento invalido'
            return context_dict
        else:
            context_dict['moveDone'] = (final==cat+MOVE_RIGHT or final==cat+MOVE_LEFT)
            context_dict['error'] = 'Movimiento invalido'
            return context_dict

    #El movimiento es realizado por el raton
    else:   
        MOVE_LEFT_BACK = 7
        MOVE_RIGHT_BACK = 9
        MOVE_LEFT_FRONT = -9
        MOVE_RIGHT_FRONT = -7

        mouse = game.mouse
        if mouse == final:
            context_dict['moveDone'] = False
            context_dict['error'] = 'mouse'
            return context_dict
        if not mouse == origin:
            context_dict['moveDone'] = False
            context_dict['error'] = 'Movimiento invalido'
            return context_dict
        if is_catThere(final, game):
            context_dict['moveDone'] = False
            context_dict['error'] = 'Movimiento invalido'
            return context_dict
        if is_border_top(mouse):
            if is_border_left(mouse):
                context_dict['moveDone'] = (final==mouse+MOVE_RIGHT_FRONT)
                context_dict['error'] = 'Movimiento invalido'
                return context_dict
            if is_border_right(mouse):
                context_dict['moveDone'] = (final==mouse+MOVE_LEFT_FRONT)
                context_dict['error'] = 'Movimiento invalido'
                return context_dict
            context_dict['moveDone'] = (final==mouse+MOVE_LEFT_FRONT or final==mouse+MOVE_RIGHT_FRONT)
            context_dict['error'] = 'Movimiento invalido'
            return context_dict
        if is_border_lower(mouse):
            if is_border_left(mouse):
                context_dict['moveDone'] = (final==mouse+MOVE_RIGHT_BACK)
                context_dict['error'] = 'Movimiento invalido'
                return context_dict
            if is_border_right(mouse):
                context_dict['moveDone'] = (final==mouse+MOVE_LEFT_BACK)
                context_dict['error'] = 'Movimiento invalido'
                return context_dict
            context_dict['moveDone'] = (final==mouse+MOVE_RIGHT_BACK or final==mouse+MOVE_LEFT_BACK)
            context_dict['error'] = 'Movimiento invalido'
        if is_border_left(mouse):
            context_dict['moveDone'] = (final==mouse+MOVE_RIGHT_BACK or final==mouse+MOVE_RIGHT_FRONT )
            context_dict['error'] = 'Movimiento invalido'
            return context_dict
        if is_border_right(mouse):
            context_dict['moveDone'] = (final==mouse+MOVE_LEFT_BACK or final==mouse+MOVE_LEFT_FRONT )
            context_dict['error'] = 'Movimiento invalido'
            return context_dict

        context_dict['moveDone'] = ( final==mouse+MOVE_LEFT_BACK or final==mouse+MOVE_LEFT_FRONT or final==mouse+MOVE_RIGHT_BACK or final==mouse+MOVE_RIGHT_FRONT)
        context_dict['error'] = 'Movimiento invalido'
        return context_dict



def get_cat(origin, game):
    """@autor Jose Manuel de Frutos Porras
        Devuelve el gato del juego game que esta en la posicion origin. Funcion auxiliar.
        Entrada: posicion origin (origin) y juego (game)
        Salida: gato correspondiente o None
    """
    if origin == game.cat1:
        return game.cat1
    elif origin == game.cat2:
        return game.cat2
    elif origin == game.cat3:
        return game.cat3
    elif origin == game.cat4:
        return game.cat4
    else:
        return None

def set_cat_final(origin,final, game):
    """@autor Jose Manuel de Frutos Porras
       Setea la posion final del gato con posion inicial origin en el juego game. Funcion auxiliar.
       Entrada: posicion origin (origin) , posicion final (final) y juego (game)
       Salida: gato correspondiente o None
    """
    if origin == game.cat1:
        game.cat1 = final
    elif  origin == game.cat2:
        game.cat2 = final
    elif origin == game.cat3:
        game.cat3 = final
    elif origin == game.cat4:
        game.cat4 = final
    else:
        return None   


def is_catThere(final,game):
    """@autor Jose Manuel de Frutos Porras
       Averigua si hay un gato en la posion final del tablero del juego game. Funcion auxiliar.
    """
    if final == game.cat1:
        return True
    elif final == game.cat2:
         return True
    elif final == game.cat3:
        return True
    elif final == game.cat4:
        return True
    elif final == game.mouse:
        return True
    else:
        return False 

def is_mouseThere(final, game):
    """@autor Jose Manuel de Frutos Porras
       Averigua si hay un raton en la posion final del tablero del juego game. Funcion auxiliar.
    """
    if final == game.mouse:
        return True
    else:
        return False

def is_someoneElse_there(final, game):
    """@autor Jose Manuel de Frutos Porras
       Averigua si hay alguna ficha en la posion final del tablero del juego game. Funcion auxiliar.
    """
    if final == game.cat1:
        return True
    elif final == game.cat2:
         return True
    elif final == game.cat3:
        return True
    elif final == game.cat4:
        return True
    elif final == game.mouse:
        return True
    else:
        return False


def is_border_top(i):
    """@autor Jose Manuel de Frutos Porras
       Averigua si una casilla del tablero forma parte del borde superior. Funcion auxiliar.
    """
    border_top_board = (56,57,58,59,60,61,62,63)
    try:
        border_top_board.index(i)
        return True
    except:
        return False

def is_border_lower(i):    
    """@autor Jose Manuel de Frutos Porras
       Averigua si una casilla del tablero forma parte del borde inferior. Funcion auxiliar.
    """
    border_lower_board = (0,1,2,3,4,5,6,7)
    try:
        border_lower_board.index(i)
        return True
    except:
        return False

def is_border_left(i):
    """@autor Jose Manuel de Frutos Porras
       Averigua si una casilla del tablero forma parte del borde izquierdo. Funcion auxiliar.
    """
    border_left_board = (0,8,16,24,32,40,48,56)
    try:
        border_left_board.index(i)
        return True
    except:
        return False

def is_border_right(i):
    """@autor Jose Manuel de Frutos Porras
       Averigua si una casilla del tablero forma parte del borde derecho. Funcion auxiliar.
    """
    border_right_board = (7,15,23,31,39,47,55,63)
    try:
        border_right_board.index(i)
        return True
    except:
        return False

@login_required
def status_turn(request):
    """@autor Jose Manuel de Frutos Porras
       Consulta a quien le toca el turno.
       Entrada: Usa las variables de sesion amIcat asi como
       game.catTurn.
       Salida: Muestra True si le toca jugar al usuario que hace la consulta, False en
       caso contrario
       Accesibilidad: esta funcion solo es ejecutable por usuarios logeados en el sistema
    """
    list_games = Game.objects.filter(id=request.session['gameID'])

    if not list_games.exists():

        return render(render, 'server/turn.html', {'myTurn': False})
    
    game = list_games[0]
    
    amIcat = request.session['amIcat']
    if game.catTurn == amIcat:
        return render(request, 'server/turn.html', {'myTurn': True})
       
    return render(request, 'server/turn.html', {'myTurn': False})


def is_my_turn(request):
    """@autor Jose Manuel de Frutos Porras
       Consulta a quien le toca el turno.
       Entrada: Usa las variables de sesion amIcat asi como
       game.catTurn.
       Salida: Muestra True si le toca jugar al usuario que hace la consulta, False en
       caso contrario
       Accesibilidad: esta funcion solo es ejecutable por usuarios logeados en el sistema
    """
    list_games = Game.objects.filter(id=request.session['gameID'])

    if not list_games.exists():
        return False
    
    game = list_games[0]
    
    amIcat = request.session['amIcat']
    if game.catTurn == amIcat:
        return True
        
    return False

    
@login_required
def status_board(request):
    """@autor Jose Manuel de Frutos Porras
       Repintado del tablero.
       Salida: array[0:63] de enteros con la posicion de gatos y raton. A los gatos se
       les asignara el valor 1, a los ratones -1 y el resto de las casillas valdran 0.
       Accesibilidad: esta funcion solo es ejecutable por usuarios logeados en el sistema
    """

    try:
        list_games = Game.objects.filter(id=request.session['gameID'])
    except: #Si no existe request.session['gameID'] porque el jugador no se ha unido todavia a ninguno de ellos devolvemos board False.
       join_game(request) #No se ha creado el juego

    list_games = Game.objects.filter(id=request.session['gameID'])
    if not list_games.exists(): #No hay juego creado
        return render(request, 'server/game_ajax_v2.html',{'board': False } )

    list_games = Game.objects.filter(id=request.session['gameID'])
        #return render(request, 'server/game_ajax_v2.html',{'board':False, 'iAmCat': request.session['amIcat']} )
    
    game = list_games[0]

    if game.mouseUser is None:
        return render(request, 'server/game_ajax_v2.html',{'board':False} )

    array=[]
    
    for i in range(64):
        array.append(0)
        
    array[game.cat1]=1
    array[game.cat2]=1
    array[game.cat3]=1
    array[game.cat4]=1
    array[game.mouse]=-1
    
    dic = is_finish(request)
    
    return render(request, 'server/game_ajax_v2.html',{'board': array, 'game': game, 'username':request.user.username, 'myTurn': is_my_turn(request), 'init': False, 'isFinish': dic['finish'], 'wins': dic['wins']})


def game(request):
    return render(request, 'server/game.html', {'username':request.user.username})

def about(request):
    return render(request, 'server/about.html', {'username':request.user.username})

def show(request):
    list_games = Game.objects.filter(mouseUser__isnull=False).order_by('-id') #Juegos no huervanos

    if not list_games.exists():
        return render(request, 'server/show.html',{'thereIsAGame': False } )

    game = list_games[0]
    request.session['show_gameId'] = game.id

    return render(request, 'server/show.html',{'thereIsAGame': True , 'username':request.user.username})

def show_final(request):
    list_games = Game.objects.filter(id = request.session['gameID']) #Juegos no huervanos

    if not list_games.exists():
        return render(request, 'server/show.html',{'thereIsAGame': False } )

    game = list_games[0]
    request.session['show_gameId'] = game.id

    return render(request, 'server/show.html',{'thereIsAGame': True , 'username':request.user.username})

def show_status_board(request):
    """@autor Jose Manuel de Frutos Porras
       Repintado del tablero.
       Salida: array[0:63] de enteros con la posicion de gatos y raton. A los gatos se
       les asignara el valor 1, a los ratones -1 y el resto de las casillas valdran 0.
       Accesibilidad: esta funcion solo es ejecutable por usuarios logeados en el sistema
    """

    list_games = Game.objects.filter(id=request.session['show_gameId'])
        #return render(request, 'server/game_ajax_v2.html',{'board':False, 'iAmCat': request.session['amIcat']} )
    
    game = list_games[0]

    array=[]
    
    for i in range(64):
        array.append(0)
        
    array[game.cat1]=1
    array[game.cat2]=1
    array[game.cat3]=1
    array[game.cat4]=1
    array[game.mouse]=-1    

    return render(request, 'server/show_ajax.html',{'board': array, 'game': game, 'username':request.user.username, 'turn': ("cat Turn" if game.catTurn else "mouse Turn")})
