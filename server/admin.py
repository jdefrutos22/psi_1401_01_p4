from django.contrib import admin

# Register your models here.
from django.contrib import admin
from server.models import Game, Move
from django.contrib.auth.models import User

class GameAdmin(admin.ModelAdmin):
    list_display = ('catUser', 'mouseUser', 'cat1', 'cat2', 'cat3', 'cat4', 'mouse', 'catTurn')

# Add in this class to customized the Admin Interface
class MoveAdmin(admin.ModelAdmin):
    list_display = ('game', 'origin', 'target')

admin.site.register(Game, GameAdmin)
admin.site.register(Move, MoveAdmin)
