from django import forms
from django.contrib.auth.models import User
from  server.models import Move

class UserForm(forms.ModelForm):
    password = forms.CharField(required=True, widget=forms.PasswordInput())

    class Meta:
        model = User
        fields = ('username', 'password')
        widgets = {
			'username': forms.TextInput(),
			'password': forms.PasswordInput()
		}

class Moveform(forms.ModelForm):
    origin = forms.IntegerField(required=True, initial=-1, widget=forms.NumberInput())
    target = forms.IntegerField(required=True, widget=forms.NumberInput())


    class Meta:
        model = Move
        fields = ('origin','target') # A list of the fields that you want to include in your form

